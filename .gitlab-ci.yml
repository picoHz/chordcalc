image: "rustlang/rust:nightly"


# Use cargo to build the project
build:
  stage: build
  script:
    - rustc --version && cargo --version  # Print version info for debugging
    - git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf "ssh://git@gitlab.com/"
    - cargo build --release
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
      - target/
  artifacts:
    paths:
      - Cargo.lock
    expire_in: 1 hour

audit:
  stage: test
  image: "alpine:latest"
  allow_failure: false
  needs: ["build"]
  script:
    - apk add cargo-audit
    - cargo-audit audit

tests:
  stage: test
  coverage: /^  lines.+?(\d+.\d+\%).*\)$/
  variables:
    LLVM_PROFILE_FILE: "coverage-%p-%m.profraw"
    RUSTFLAGS: "-C instrument-coverage"
  image: "registry.gitlab.com/eutampieri/ci-tools/rust_codecov:latest"
  script:
    - cargo test --lib -- -Z unstable-options --format json --report-time | tee tests.json
    - grcov . --binary-path ./target/debug/ -s . -t html --branch --ignore-not-existing -o ./coverage/
    - grcov . --binary-path ./target/debug/ -s . -t lcov --branch --ignore-not-existing -o coverage.lcov
    - python3 /usr/local/lib/python3.7/dist-packages/lcov_cobertura/lcov_cobertura.py coverage.lcov
    - lcov --summary coverage.lcov
  cache:
    key: "tests_$CI_COMMIT_REF_SLUG"
    paths:
      - target/
  artifacts:
    paths:
      - "*.json"
      - 'coverage'
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    expire_in: 1 hour
tests_report:
  stage: test
  image: "registry.gitlab.com/eutampieri/ci-tools/cargo2junit:latest"
  script:
    - cat tests.json | /tools/cargo2junit > tests.xml
  needs: ["tests"]
  artifacts:
    reports:
      junit: ["tests.xml"]
tests_badges:
  stage: test
  image: "python:latest"
  needs: ["tests"]
  script:
    - git clone https://gitlab.com/eutampieri/ci-tools.git
    - ci-tools/rust_tests_percentage.py tests < tests.json
  artifacts:
    paths:
      - "*.svg"
    expire_in: 1 hour

doccov:
  stage: test
  script:
    - git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf "ssh://git@gitlab.com/"
    - RUSTDOCFLAGS="--show-coverage --output-format json -Z unstable-options" cargo doc --document-private-items --no-deps --workspace --all-features > doccov.json
  cache:
    key: "docs_$CI_COMMIT_REF_SLUG"
    paths:
      - target/
  artifacts:
    paths:
      - doccov.json
    expire_in: 1 hour

doccov_badges:
  stage: test
  image: "python:latest"
  needs: ["doccov"]
  script:
    - git clone https://gitlab.com/eutampieri/ci-tools.git
    - ci-tools/rust_doccov_badges.py < doccov.json
  artifacts:
    paths:
      - "*.svg"
    expire_in: 1 hour

pages: # Publish cargo doc output
  stage: test
  script:
    - cargo doc --no-deps --workspace --all-features
    - cp -r target/doc public
    - echo "<a href=\"eprex_parser\">documentation</a>" > public/index.html
  cache:
    key: "docs_$CI_COMMIT_REF_SLUG"
    paths:
      - target/
  artifacts:
    paths:
      - public
