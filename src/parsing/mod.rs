use core::str::FromStr;

use crate::notes::{Accidental, MusicalNote, Note};

mod accidentals;
mod chords;
mod notes;

pub use chords::parse_chord;

#[derive(PartialEq, Debug)]
/// Parsing error
pub enum Error {
    /// A note could not be extracted from the provided string.
    UnknownNote,
    /// An accidental could not be extracted from the provided string.
    /// A mode could not be extracted from the provided string.
    UnknownAccidental,
    UnknownMode,
    /// The string has been parsed successfuly but it wasn't conmpletely consumed.
    UnusedParts,
}

fn parse_musical_note(input: &str) -> Result<(usize, MusicalNote), Error> {
    let mut index = 0;
    let mut parsed;
    let note = {
        parsed = 3.min(input.len());
        let s = &input[index..(index + parsed).min(input.len())];
        s
    }
    .parse::<Note>()
    .or_else(|_| {
        parsed = 2.min(input.len());
        input[index..(index + parsed).min(input.len())].parse::<Note>()
    })
    .or_else(|_| {
        parsed = 1.min(input.len());
        input[index..(index + parsed).min(input.len())].parse::<Note>()
    })?;
    index = parsed;
    let accidental = {
        parsed = (index + 2).min(input.len());
        &input[index..parsed]
    }
    .parse::<Accidental>()
    .or_else(|_| {
        parsed = (index + 1).min(input.len());
        input[index..parsed].parse::<Accidental>()
    })
    .or_else(|_| {
        parsed = (index + 0).min(input.len());
        input[index..parsed].parse::<Accidental>()
    })?;
    index = parsed;
    Ok((index, MusicalNote(note, accidental)))
}

impl FromStr for MusicalNote {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (i, n) = parse_musical_note(s)?;
        let l = s.len();
        if l != i {
            Err(Error::UnusedParts)
        } else {
            Ok(n)
        }
    }
}
#[cfg(test)]
mod tests {
    use crate::{
        notes::{Accidental, MusicalNote, Note},
        ParsingError,
    };

    #[test]
    fn parse_ascii() {
        assert_eq!(
            Ok(MusicalNote(Note::G, Accidental::Sharp)),
            "Sol#".parse::<MusicalNote>()
        );
        assert_eq!(
            Ok(MusicalNote(Note::G, Accidental::Sharp)),
            "G#".parse::<MusicalNote>()
        );
        assert_eq!(
            Ok(MusicalNote(Note::C, Accidental::Natural)),
            "Do".parse::<MusicalNote>()
        );
        assert_eq!(
            Err(ParsingError::UnknownNote),
            "Pizza".parse::<MusicalNote>()
        );
        assert_eq!(Err(ParsingError::UnknownNote), "".parse::<MusicalNote>());
        assert_eq!(Err(ParsingError::UnknownNote), "Pi".parse::<MusicalNote>());
        assert_eq!(Err(ParsingError::UnusedParts), "Ei".parse::<MusicalNote>());
    }
}
