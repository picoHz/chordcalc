use core::str::FromStr;

use crate::notes::Note;

use super::Error;

impl FromStr for Note {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Self::A),
            "B" => Ok(Self::B),
            "C" => Ok(Self::C),
            "D" => Ok(Self::D),
            "E" => Ok(Self::E),
            "F" => Ok(Self::F),
            "G" => Ok(Self::G),
            "DO" => Ok(Self::C),
            "Do" => Ok(Self::C),
            "do" => Ok(Self::C),
            "RE" => Ok(Self::D),
            "Re" => Ok(Self::D),
            "re" => Ok(Self::D),
            "MI" => Ok(Self::E),
            "Mi" => Ok(Self::E),
            "mi" => Ok(Self::E),
            "FA" => Ok(Self::F),
            "Fa" => Ok(Self::F),
            "fa" => Ok(Self::F),
            "SOL" => Ok(Self::G),
            "Sol" => Ok(Self::G),
            "sol" => Ok(Self::G),
            "LA" => Ok(Self::A),
            "La" => Ok(Self::A),
            "la" => Ok(Self::A),
            "SI" => Ok(Self::B),
            "Si" => Ok(Self::B),
            "si" => Ok(Self::B),
            _ => Err(Error::UnknownNote),
        }
    }
}

#[cfg(test)]
#[cfg(test)]
mod tests {
    use crate::{notes::Note, parsing::Error};

    #[test]
    fn parse_notes() {
        let result = &[
            Note::A,
            Note::B,
            Note::C,
            Note::D,
            Note::E,
            Note::F,
            Note::G,
        ];
        for (note, expected) in ["A", "B", "C", "D", "E", "F", "G"]
            .iter()
            .zip(result.iter())
        {
            let parsed = note.parse();
            assert_eq!(parsed, Ok(*expected));
        }
    }
    #[test]
    fn parse_romance_notes() {
        let result = &[
            Note::A,
            Note::B,
            Note::C,
            Note::D,
            Note::E,
            Note::F,
            Note::G,
        ];
        for (note, expected) in ["La", "Si", "Do", "Re", "Mi", "Fa", "Sol"]
            .iter()
            .zip(result.iter())
        {
            let parsed = note.parse();
            assert_eq!(parsed, Ok(*expected));
            let parsed = note.to_uppercase().parse();
            assert_eq!(parsed, Ok(*expected));
            let parsed = note.to_ascii_lowercase().parse();
            assert_eq!(parsed, Ok(*expected));
        }
    }
    #[test]
    fn parse_unexisting_notes() {
        for note in &["H", "ABC"] {
            let parsed: Result<Note, Error> = note.parse();
            assert_eq!(parsed, Err(Error::UnknownNote));
        }
    }
}
