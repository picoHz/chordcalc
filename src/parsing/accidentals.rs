use core::str::FromStr;

use crate::notes::Accidental;

use super::Error;

impl FromStr for Accidental {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "#" => Ok(Self::Sharp),
            "b" => Ok(Self::Flat),
            "♭" => Ok(Self::Flat),
            "♯" => Ok(Self::Sharp),
            "" => Ok(Self::Natural),
            "bb" => Ok(Self::DoubleFlat),
            "𝄫" => Ok(Self::DoubleFlat),
            "𝄪" => Ok(Self::DoubleSharp),
            "##" => Ok(Self::DoubleSharp),
            _ => Err(Error::UnknownAccidental),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{notes::Accidental, parsing::Error};

    #[test]
    fn parse_ascii() {
        let result = &[
            Accidental::Sharp,
            Accidental::DoubleSharp,
            Accidental::Flat,
            Accidental::DoubleFlat,
            Accidental::Natural,
        ];
        for (note, expected) in ["#", "##", "b", "bb", ""].iter().zip(result.iter()) {
            let parsed = note.parse();
            assert_eq!(parsed, Ok(*expected));
        }
    }
    #[test]
    fn parse_unicode() {
        let result = &[
            Accidental::Sharp,
            Accidental::DoubleSharp,
            Accidental::Flat,
            Accidental::DoubleFlat,
        ];
        for (note, expected) in ["♯", "𝄪", "♭", "𝄫"].iter().zip(result.iter()) {
            let parsed = note.parse();
            assert_eq!(parsed, Ok(*expected));
        }
    }
    #[test]
    fn parse_unexisting_accidentals() {
        for note in &["H", "#b"] {
            let parsed: Result<Accidental, Error> = note.parse();
            assert_eq!(parsed, Err(Error::UnknownAccidental));
        }
    }
}
