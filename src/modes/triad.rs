use core::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Mode {
    Major,
    Minor,
    Augmented,
    Diminished,
}
impl crate::Mode for Mode {
    fn make_chord(&self, tonality: crate::notes::MusicalNote) -> crate::chords::Chord {
        let base = tonality;
        match self {
            Mode::Major => [base, base + 4, base + 7],
            Mode::Minor => [base, base + 3, base + 7],
            Mode::Augmented => [base, base + 4, base + 8],
            Mode::Diminished => [base, base + 4, base + 6],
        }
        .into()
    }
}

impl Display for Mode {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Mode::Major => write!(f, ""),
            Mode::Minor => write!(f, "m"),
            Mode::Augmented => write!(f, "aug"),
            Mode::Diminished => write!(f, "dim"),
        }
    }
}
