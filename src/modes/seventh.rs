use core::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Mode {
    Major,
    Minor,
    Dominant,
    HalfDiminished,
    Diminished,
    MinorMajor,
    Augmented,
    AugmentedMajor,
    DiminishedMajor,
    DominantFlatFive,
    MajorFlatFive,
}

impl crate::Mode for Mode {
    fn make_chord(&self, tonality: crate::notes::MusicalNote) -> crate::chords::Chord {
        let base = tonality;
        match self {
            Mode::Major => [base, base + 4, base + 7, base + 11],
            Mode::Minor => [base, base + 3, base + 7, base + 10],
            Mode::Dominant => [base, base + 4, base + 7, base + 10],
            Mode::HalfDiminished => [base, base + 3, base + 6, base + 10],
            Mode::Diminished => [base, base + 4, base + 7, base + 11],
            Mode::MinorMajor => [base, base + 3, base + 7, base + 11],
            Mode::Augmented => [base, base + 4, base + 8, base + 10],
            Mode::AugmentedMajor => [base, base + 4, base + 8, base + 11],
            Mode::DiminishedMajor => [base, base + 3, base + 6, base + 11],
            Mode::DominantFlatFive => [base, base + 4, base + 6, base + 10],
            Mode::MajorFlatFive => [base, base + 4, base + 6, base + 11],
        }
        .into()
    }
}

impl Display for Mode {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Mode::Major => write!(f, "M⁷"),
            Mode::Minor => write!(f, "m⁷"),
            Mode::Dominant => write!(f, "⁷"),
            Mode::HalfDiminished => write!(f, "ø⁷"),
            Mode::Diminished => write!(f, "dim⁷"),
            Mode::MinorMajor => write!(f, "mM⁷"),
            Mode::Augmented => write!(f, "aug⁷"),
            Mode::AugmentedMajor => write!(f, "+M⁷"),
            Mode::DiminishedMajor => todo!(),
            Mode::DominantFlatFive => todo!(),
            Mode::MajorFlatFive => todo!(),
        }
    }
}
