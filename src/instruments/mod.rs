mod ukulele;
pub use ukulele::Ukulele;

use crate::notes::MusicalNote;
pub trait Instrument: Default {
    type Tabulature;
    fn play(chord: &[MusicalNote]) -> Self::Tabulature;
}
