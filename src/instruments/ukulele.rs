use crate::notes::MusicalNote;

use super::Instrument;

const NOTES: [MusicalNote; 4] = [
    MusicalNote(crate::notes::Note::G, crate::notes::Accidental::Natural),
    MusicalNote(crate::notes::Note::C, crate::notes::Accidental::Natural),
    MusicalNote(crate::notes::Note::E, crate::notes::Accidental::Natural),
    MusicalNote(crate::notes::Note::A, crate::notes::Accidental::Natural),
];

#[derive(Default)]
pub struct Ukulele {}
impl Ukulele {}

impl Instrument for Ukulele {
    type Tabulature = [Option<u8>; 4];

    fn play(chord: &[MusicalNote]) -> Self::Tabulature {
        let mut res = [None; 4];
        for (i, string) in NOTES.iter().enumerate() {
            let note = chord
                .iter()
                .map(|x| (*x - *string).0.rem_euclid(12) as u8)
                .min();
            res[i] = note;
        }
        res
    }
}
#[cfg(test)]
mod tests {
    use core::borrow::Borrow;

    use crate::{chords::Chord, modes::triad::Mode, notes::MusicalNote};

    use super::Ukulele;

    #[test]
    fn tab_c_maj() {
        use crate::instruments::Instrument;
        //let uke = Ukulele::default();
        let tab = Ukulele::play(
            Chord::from((
                MusicalNote(crate::notes::Note::C, crate::notes::Accidental::Natural),
                Mode::Major,
            ))
            .borrow(),
        );
        assert_eq!(tab, [Some(0), Some(0), Some(0), Some(3)]);
    }
}
